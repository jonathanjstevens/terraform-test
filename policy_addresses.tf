# Load and create the address and address group objects
# Shows a mechanism by which different object types (addresses and address groups) can 
# be collated across multiple YAML files with some mechanisms for uniqueness
# Enables policy to be defined more modularly


locals {
  # Load and combine all the yaml files in a specific directory
  address_combined_yaml = flatten([
    for i in fileset(path.module, "policy_objects/addresses/*.yaml") :
      yamldecode(file(i))
  ])
  # That created an array of maps, with each map having namespace, (opt) addresses and (opt) address_groups
  # also forcing lowercase

  # Extract an array of all the addresses
  namespaced_addresses = flatten([
    for i in local.address_combined_yaml : [
      for l in i.addresses : {
        name        = lower(format("%s_%s", i.namespace, l.name))
        description = l.description
        address     = l.address
      }
    ]
  ])

  # Create a map of the previous array (for lookup purposes)
  # Note that any duplicates will override any previous declaration
  # addresses are not checked for uniqueness, but that should be tolerated
  namespaced_addresses_map = { for i in local.namespaced_addresses : i.name => i }


  # Same again now for the address_groups
  # Note that the members are updated to includes the namespace prefix, 
  # to match properly with the namespaced addresses
  namespaced_address_groups = flatten([
    for i in local.address_combined_yaml : [
      for l in i.address_groups : {
        name        = lower(format("%s_%s", i.namespace, l.name))
        description = l.description
        members = [
          for m in l.members :
            lower(format("%s_%s", i.namespace, m))
        ]
      }
    ]
  ])
  namespaced_address_groups_map = { for i in local.namespaced_address_groups : i.name => i }


}


output "op_address_combined_yaml" {
  value = local.address_combined_yaml
}

output "op_namespaced_addresses" {
  value = local.namespaced_addresses
}

output "op_namespaced_addresses_map" {
  value = local.namespaced_addresses_map
}

output "op_namespaced_address_groups_map" {
  value = local.namespaced_address_groups_map
}
