# Creates an output containing the value of HELLO_WORLD
# This is passed from Gitlab to runner Terraform container using TF_VAR environment variables
#
# The variable is set in the repository, Settings, CI/CD variables
#
# This is set in the Gitlab runner config
# before_script:
#   - export TF_VAR_HELLO_WORLD=${HELLO_WORLD}


variable "HELLO_WORLD" {
  type    = string
  default = "This is the default value."
}


output "hello_world" {
  value = var.HELLO_WORLD
}
